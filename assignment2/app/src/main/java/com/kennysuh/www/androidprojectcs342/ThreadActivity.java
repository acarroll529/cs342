package com.kennysuh.www.androidprojectcs342;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Aidan on 4/8/2015.
 */
public class ThreadActivity extends ActionBarActivity implements View.OnClickListener {

    private final static int MESSAGE_TYPE_PROGRESS = 1;
    private final static int MESSAGE_TYPE_DONE = 2;

    private long topInteger;
    private PrimeHandler handler;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);

        // Set up the button
        Button button = (Button)this.findViewById(R.id.go_button);
        button.setOnClickListener(this);

        // Create the handler to be used by the background thread.
        this.handler = new PrimeHandler();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;
        }
        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        // Drop the keyboard and get N
        this.hideKeyboard();
        EditText integerTextField = (EditText)this.findViewById(R.id.integer_edit_text);
        String integerString = integerTextField.getText().toString();
        this.topInteger = Long.parseLong(integerString);

        TextView resultTextView = (TextView)this.findViewById(R.id.result_text_view);
        resultTextView.setText(R.string.computing_caption);

        // Create the worker thread and start it up.
        Thread thread = new Thread(new PrimeRunnable());
        thread.setDaemon(true);
        thread.start();
    }

    public void hideKeyboard() {
        EditText myEditText = (EditText)this.findViewById(R.id.integer_edit_text);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

    private class PrimeHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            TextView resultTextView = (TextView)findViewById(R.id.result_text_view);

            switch (message.what) {
                case ThreadActivity.MESSAGE_TYPE_PROGRESS:
                    resultTextView.setText("Computing... \nCurrent prime factors: " + ((ArrayList<Long>) message.obj).toString());
                    break;

                case ThreadActivity.MESSAGE_TYPE_DONE:
                    resultTextView.setText("The prime factors: " + ((ArrayList<Long>) message.obj).toString());
                    break;
            }
        }
    }

    private class PrimeRunnable implements Runnable {
        public void run() {
            ArrayList<Long> primes = new ArrayList<Long>();
            for (long k = 2; k < topInteger && topInteger >= 2; ++k) {
                if (topInteger % k == 0 && this.isPrime(k)) {
                    primes.add(k);
                    topInteger = topInteger/k;
                    k = 2;

                    Message message = new Message();
                    message.what = ThreadActivity.MESSAGE_TYPE_PROGRESS;
                    message.obj = primes;
                    handler.sendMessage(message);

                }

            }

            Message message = new Message();
            message.obj = primes;
            message.what = ThreadActivity.MESSAGE_TYPE_DONE;
            handler.sendMessage(message);


        }

        // Not quite the maximally stupid primality checker, but close.
        // This is intended to be slow, so we can easily demonstrate a
        // time-consuming background thread.
        private boolean isPrime(long n) {
            long squareRoot = (long)Math.sqrt((double)n);
            if (n % 2 == 0) {
                return false;
            }

            for (long k = 3; k <= squareRoot; k += 2) {
                if (n % k == 0) {
                    return false;
                }
            }

            return true;
        }
    }
}
