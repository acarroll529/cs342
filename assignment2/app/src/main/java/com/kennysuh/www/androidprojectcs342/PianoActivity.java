package com.kennysuh.www.androidprojectcs342;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.media.AudioManager;
import android.media.SoundPool;

public class PianoActivity extends ActionBarActivity implements View.OnClickListener {

    SoundPool sp;
    AudioManager am;
    float actVolume, maxVolume, volume;
    private int cid, did, eid, fid, gid, aid, bid, hicid, hidid, hieid, hifid, higid;
    int theID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_piano);

        Button buttonc = (Button)this.findViewById(R.id.c);
        buttonc.setOnClickListener(this);
        Button buttond = (Button)this.findViewById(R.id.d);
        buttond.setOnClickListener(this);
        Button buttone = (Button)this.findViewById(R.id.e);
        buttone.setOnClickListener(this);
        Button buttonf = (Button)this.findViewById(R.id.f);
        buttonf.setOnClickListener(this);
        Button buttong = (Button)this.findViewById(R.id.g);
        buttong.setOnClickListener(this);
        Button buttona = (Button)this.findViewById(R.id.a);
        buttona.setOnClickListener(this);
        Button buttonb = (Button)this.findViewById(R.id.b);
        buttonb.setOnClickListener(this);
        Button buttonhic = (Button)this.findViewById(R.id.hic);
        buttonhic.setOnClickListener(this);
        Button buttonhid = (Button)this.findViewById(R.id.hid);
        buttonhid.setOnClickListener(this);
        Button buttonhie = (Button)this.findViewById(R.id.hie);
        buttonhie.setOnClickListener(this);
        Button buttonhif = (Button)this.findViewById(R.id.hif);
        buttonhif.setOnClickListener(this);
        Button buttonhig = (Button)this.findViewById(R.id.hig);
        buttonhig.setOnClickListener(this);

        am = (AudioManager) getSystemService(AUDIO_SERVICE);
        actVolume = (float) am.getStreamVolume(AudioManager.STREAM_MUSIC);
        maxVolume = (float) am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        volume = actVolume / maxVolume;
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);

        sp = new SoundPool(12, AudioManager.STREAM_MUSIC, 100);
        sp.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int i, int i2) {

            }
        });

        cid = sp.load(this, R.raw.pianoc, 1);
        did = sp.load(this, R.raw.pianod, 1);
        eid = sp.load(this, R.raw.pianoe, 1);
        fid = sp.load(this, R.raw.pianof, 1);
        gid = sp.load(this, R.raw.pianog, 1);
        aid = sp.load(this, R.raw.pianoa, 1);
        bid = sp.load(this, R.raw.pianob, 1);
        hicid = sp.load(this, R.raw.pianohic, 1);
        hidid = sp.load(this, R.raw.pianohid, 1);
        hieid = sp.load(this, R.raw.pianohie, 1);
        hifid = sp.load(this, R.raw.pianohif, 1);
        higid = sp.load(this, R.raw.pianohig, 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
            return true;
        }
        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class SoundThread implements Runnable {

        int id;
        SoundPool sp;
        float volume;

        public SoundThread(SoundPool soundPool, int noteid, float vol) {
            id = noteid;
            sp = soundPool;
            volume = vol;
        }

        public void run() {
            sp.play(id, volume, volume, 1, 0, 1f);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.c:
                theID = cid;
                break;
            case R.id.d:
                theID = did;
                break;
            case R.id.e:
                theID = eid;
                break;
            case R.id.f:
                theID = fid;
                break;
            case R.id.g:
                theID = gid;
                break;
            case R.id.a:
                theID = aid;
                break;
            case R.id.b:
                theID = bid;
                break;
            case R.id.hic:
                theID = hicid;
                break;
            case R.id.hid:
                theID = hidid;
                break;
            case R.id.hie:
                theID = hieid;
                break;
            case R.id.hif:
                theID = hifid;
                break;
            case R.id.hig:
                theID = higid;
                break;
        }
        new Thread(new SoundThread(sp, theID, volume)).start();
    }
}
