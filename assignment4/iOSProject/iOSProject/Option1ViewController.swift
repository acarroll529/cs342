//
//  Option1ViewController.swift
//  iOSProject
//
//  Created by mobiledev on 4/20/15.
//  Copyright (c) 2015 mobiledev. All rights reserved.
//

import UIKit
import Foundation

class Option1ViewController: UIViewController{
    
    var watch = Stopwatch()
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var stop: UIButton!
    @IBOutlet weak var go: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        go.backgroundColor = UIColor.blackColor()
        go.setTitle("Go", forState: UIControlState.Normal)
        go.addTarget(self, action: "goAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
        stop.backgroundColor = UIColor.blackColor()
        stop.setTitle("Stop", forState: UIControlState.Normal)
        stop.addTarget(self, action: "stopAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.addSubview(go)
        self.view.addSubview(stop)
        
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func goAction(sender:UIButton!){
        watch.start()
    }
    
    func stopAction(sender:UIButton!){
        headingLabel.text = watch.stop().description
    }
}
@objc class Stopwatch: NSObject {
    private var startTime: NSDate?
    
    var isRunning: Bool {
        return startTime != nil
    };
    
    func start() {
        startTime = NSDate()
    }
    
    func stop() -> NSTimeInterval {
        if let time = startTime {
            startTime = nil
            
            return NSDate().timeIntervalSinceDate(time)
        }
        else {
            return 0
        }
    }
}