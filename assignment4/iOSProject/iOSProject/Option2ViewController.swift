//
//  Option2ViewController.swift
//  iOSProject
//
//  Created by mobiledev on 4/21/15.
//  Copyright (c) 2015 mobiledev. All rights reserved.

import UIKit
class Option2ViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = NSURL(string: "http://developer.android.com/index.html")
        let request = NSURLRequest(URL: url!)
        webView.loadRequest(request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
