//
//  Option3ViewController.swift
//  iOSProject
//
//  Created by mobiledev on 4/21/15.
//  Copyright (c) 2015 mobiledev. All rights reserved.
//

import UIKit
class Option3ViewController: UIViewController {
    
    var isTyping = false
    var firstNum: Int? = 0
    var secondNum: Int? = 0
    var op = ""
    
    @IBOutlet weak var calculatorDisplay: UILabel!
    @IBAction func numberTapped(sender: AnyObject) {
        var num = sender.currentTitle
        if isTyping {
            calculatorDisplay.text! += num!!
        } else {
            calculatorDisplay.text = num
            isTyping = true
        }
    }
    @IBAction func operationTapped(sender: AnyObject) {
        isTyping = false
        firstNum = calculatorDisplay.text!.toInt()
        calculatorDisplay.text! += "+"
    }
    @IBAction func equalsTapped(sender: AnyObject) {
        isTyping = false
        secondNum = calculatorDisplay.text!.toInt()
        var total = firstNum! + secondNum!
        calculatorDisplay.text = total.description
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
