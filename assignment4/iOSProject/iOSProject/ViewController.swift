//
//  ViewController.swift
//  iOSProject
//
//  Created by mobiledev on 4/20/15.
//  Copyright (c) 2015 mobiledev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var optionsTableView: UITableView?
    
    let cellIdentifier = "ListCell"
    let options = ["Stopwatch", "Web View", "Calc"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.optionsTableView?.delegate = self
        self.optionsTableView?.dataSource = self
        self.optionsTableView?.tableFooterView = UIView(frame: CGRect.zeroRect)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return self.options.count
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        
        cell.textLabel?.text = self.options[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selection = self.options[indexPath.row]
        if selection == "Stopwatch" {
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            performSegueWithIdentifier("op1segue", sender: cell)
        } else if selection == "Web View" {
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            performSegueWithIdentifier("op2segue", sender: cell)
        } else if selection == "Calc" {
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            performSegueWithIdentifier("op3segue", sender: cell)
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier! == "op1segue" {
            var vc2 = segue.destinationViewController as Option1ViewController
            return
        } else if segue.identifier! == "op2segue" {
            var vc2 = segue.destinationViewController as Option2ViewController
            return
        } else if segue.identifier! == "op3segue" {
            var vc3 = segue.destinationViewController as Option3ViewController
            return
        } else if segue.identifier! == "aboutsegue" {
            var vc4 = segue.destinationViewController as AboutViewController
        }
    }

}

